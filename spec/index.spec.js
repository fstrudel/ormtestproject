var Request = require("request");

describe("Server", () => {
    var server;
    beforeAll(() => {
        server = require("../index");
    });
    // afterAll(() => {
    //     server.close();
    // });
    describe("GET /", () => {
        var data = {};
        beforeAll((done) => {
            Request.get("http://localhost:8080/", (error, response, body) => {
                data.status = response.statusCode;
                data.body = body;
                done();
            });
        });
        it("Status 200", () => {
            expect(data.status).toBe(200);
        });
        it("Body", () => {
            expect(data.body).toBe('"home"');
        });
    });
    describe("GET /users", () => {
        var data = {};
        beforeAll((done) => {
            Request.get("http://localhost:8080/users",(error,response,body) => {
                data.status = response.statusCode;
                data.body = body;
                done();
            });
        });
        it("Status 200", () => {
            expect(data.status).toBe(200);
        });
        it("Body", () => {
            let jsonObject = JSON.parse(data.body);
            console.log(data.body);
            console.log(jsonObject.id);
            expect(data.body['id']).toBe(1);
        });
    });
});

