var Sequelize = require('sequelize');

// load models
var models = [
    'userModel'
];

models.forEach(function (model) {
    console.log(__dirname + '/models/' + model)
    module.exports[model] = sequelize.import(__dirname + '/' + model);
});