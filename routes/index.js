
    function respond(req, res, next) {
        res.send('hello ' + req.params.name);
        next();
    }

    server.get('/hello/:name', respond);
    server.head('/hello/:name', respond);
    
    server.get('/', function (req, res, next) {
        res.send('home')
        return next();
    });

    server.get('/users', function (req, res, next) {
        var User = models.userModel;
        User.findAll({raw: true}).then(function (users) {
               res.json(users);
          });
        return next();
    });
