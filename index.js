'use-strict'
const config = require('./config'),
    Sequelize = require('sequelize'),
    bunyan = require('bunyan'),
    winston = require('winston'),
    bunyanWinston = require('bunyan-winston-adapter'),
    restify = require('restify');

// Sets up logging .
global.log = new winston.Logger({
    transports: [
        new winston.transports.Console({
            level: 'info',
            timestamp: () => {
                return new Date().toString()
            },
            json: true
        }),
    ]
})

global.sequelize = new Sequelize('database', 'username', '', {
    dialect: 'sqlite',
    storage: 'data.sqlite',
    logging: null
});


global.server = restify.createServer({
    name: config.name,
    version: config.version,
    log: bunyanWinston.createAdapter(log)
});

server.pre(restify.plugins.pre.userAgentConnection());

server.use(restify.plugins.bodyParser({ mapParams: true }))
server.use(restify.plugins.acceptParser(server.acceptable))
server.use(restify.plugins.queryParser({ mapParams: true }))
server.use(restify.plugins.fullResponse())

server.on('uncaughtException', (req, res, route, err) => {
    log.error(err.stack)
    res.send(err)
})

server.listen(config.port, function () {
    sequelize
        .authenticate()
        .then(() => {
            log.info(
                'Connection to the database has been established successfully.'
            )
        })
        .catch(err => {
            log.info(
                'Unable to connect to the database:', err
            )
        });

    log.info(
        '%s v%s ready to accept connection on port %s in %s environment.',
        server.name,
        config.version,
        config.port,
        config.env
    )

    require('./routes')
    global.models = require('./models');
    var User = models.userModel;

    User.sync({ force: true }).then(() => {
        // Table created
        User.create({
            first_name: "John",
            last_name: "Hancock"
        });
        // return User.findAll().then(users => {
        //    users.forEach(function(element){
        //     log.info(
        //         element.first_name,
        //         element.last_name
        //     )
        //    });
        // });
    });
})